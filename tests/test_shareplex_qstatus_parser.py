from src.shareplex_qstatus_parser import _get_queue_source_from_line, _get_queue_type_from_line, _is_name_line, \
    parse_shareplex_output


def test_if_name_does_exits_in_each_line():
    line = " Name:  Q_TO_AWS (o.SPLEX_TESTING_ONPREM-o.SPLEX_TESTING_AWS) (Post queue)"
    expected_result = True
    input_value = _is_name_line(line)
    assert input_value == expected_result


def test_if_name_does_not_exits_in_each_line():
    line = "Number of messages:          1 (Age         0 min; Size          4 mb)"
    expected_result = False
    input_value = _is_name_line(line)
    assert input_value == expected_result


def test_get_queue_name_from_each_line():
    line = "  Name:  Q_TO_AWS (o.SPLEX_TESTING_ONPREM-o.SPLEX_TESTING_AWS) (Post queue)"
    expected_result = "Post"
    input_value = _get_queue_type_from_line(line)
    assert input_value == expected_result


def test_get_queue_source_from_line_when_input_is_valid_return_queue_name():
    line = "  Name:  Q_TO_AWS (o.SPLEX_TESTING_ONPREM-o.SPLEX_TESTING_AWS) (Post queue)"
    expected_result = "o.SPLEX_TESTING_ONPREM"
    input_value = _get_queue_source_from_line(line)
    assert input_value == expected_result


def test_get_queue_type_from_line_when_input_is_valid_return_queue_name():
    line = "  Name:  Q_TO_AWS (o.SPLEX_TESTING_ONPREM-o.SPLEX_TESTING_AWS) (Post queue)"
    expected_result = "o.SPLEX_TESTING_ONPREM"
    input_value = _get_queue_source_from_line(line)
    assert input_value == expected_result


def test_get_queue_source_from_line_when_line_has_no_queue_name_result_queue_type():
    line = "Name:  o.SPLEX_TESTING_ONPREM (Capture queue)"
    expected_result = "o.SPLEX_TESTING_ONPREM"
    input_value = _get_queue_source_from_line(line)
    assert input_value == expected_result


def test_parse_shareplex_output_when_inputs_valid_return_parsed_information():
    queues_information = parse_shareplex_output(_get_test_shareplex_input_data())

    assert len(queues_information) == 4

    assert queues_information[0].name == 'Q_TO_AWS'
    assert queues_information[0].source == 'o.SPLEX_TESTING_ONPREM'
    assert queues_information[0].type == 'Post'
    assert queues_information[0].number_of_messages == 1
    assert queues_information[0].backlog_messages == 2

    assert queues_information[1].name == 'o.SPLEX_TESTING_ONPREM'
    assert queues_information[1].source == 'o.SPLEX_TESTING_ONPREM'
    assert queues_information[1].type == 'Capture'
    assert queues_information[1].number_of_messages == 3
    assert queues_information[1].backlog_messages == 4

    assert queues_information[2].name == 'o.SPLEX_TESTING_AWS'
    assert queues_information[2].source == 'o.SPLEX_TESTING_AWS'
    assert queues_information[2].type == 'Capture'
    assert queues_information[2].number_of_messages == 5
    assert queues_information[2].backlog_messages == 6

    assert queues_information[3].name == 'Q_TO_ONPREM'
    assert queues_information[3].source == 'o.SPLEX_TESTING_AWS'
    assert queues_information[3].type == 'Post'
    assert queues_information[3].number_of_messages == 7
    assert queues_information[3].backlog_messages == 8


def _get_test_shareplex_input_data():
    return """
*******************************************************
* SharePlex Command Utility
* Copyright 2019 Quest Software Inc.
* ALL RIGHTS RESERVED.
* Protected by U.S. Patents: 7,461,103 and 7,065,538
*******************************************************

sp_ctrl (nlam01-vtsplx01:2100)> qstatus

Queues Statistics for nlam01-vtsplx01
  Name:  Q_TO_AWS (o.SPLEX_TESTING_ONPREM-o.SPLEX_TESTING_AWS) (Post queue)
    Number of messages:          1 (Age         0 min; Size          4 mb)
    Backlog (messages):          2 (Age         0 min)

  Name:  o.SPLEX_TESTING_ONPREM (Capture queue)
    Number of messages:          3 (Age         0 min; Size          2 mb)
    Backlog (messages):          4 (Age         0 min)

  Name:  o.SPLEX_TESTING_AWS (Capture queue)
    Number of messages:          5 (Age         0 min; Size          4 mb)     0 min)
    Backlog (messages):          6 (Age         0 min)    

  Name:  Q_TO_ONPREM (o.SPLEX_TESTING_AWS-o.SPLEX_TESTING_ONPREM) (Post queue)
    Number of messages:          7 (Age         0 min; Size          1 mb)
    Backlog (messages):          8 (Age         0 min)

"""
