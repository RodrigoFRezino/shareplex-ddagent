from mock import call, patch

from shareplex_check import ShareplexCheck


@patch.object(ShareplexCheck, '_get_qstatus_output')
@patch.object(ShareplexCheck, 'gauge')
def test_shareplex_check_check_when_send_metrics_to_dd_should_send_current_status(gauge_mock, get_output_mock):
    get_output_mock.return_value = _get_test_shareplex_input_data()

    shareplex_check = ShareplexCheck()
    shareplex_check.check(None)

    gauge_mock.assert_has_calls([
        call('shareplex.qstatus.post.number_of_messages', 1, tags=['QUEUE_IDENTIFIER:Q_TO_AWS']),
        call('shareplex.qstatus.post.backlog_messages', 2, tags=['QUEUE_IDENTIFIER:Q_TO_AWS']),
        call('shareplex.qstatus.capture.number_of_messages', 3, tags=['QUEUE_IDENTIFIER:o.SPLEX_TESTING_ONPREM']),
        call('shareplex.qstatus.capture.backlog_messages', 4, tags=['QUEUE_IDENTIFIER:o.SPLEX_TESTING_ONPREM']),
        call('shareplex.qstatus.capture.number_of_messages', 5, tags=['QUEUE_IDENTIFIER:o.SPLEX_TESTING_AWS']),
        call('shareplex.qstatus.capture.backlog_messages', 6, tags=['QUEUE_IDENTIFIER:o.SPLEX_TESTING_AWS']),
        call('shareplex.qstatus.post.number_of_messages', 7, tags=['QUEUE_IDENTIFIER:Q_TO_ONPREM']),
        call('shareplex.qstatus.post.backlog_messages', 8, tags=['QUEUE_IDENTIFIER:Q_TO_ONPREM'])
    ])


def _get_test_shareplex_input_data():
    return """
*******************************************************
* SharePlex Command Utility
* Copyright 2019 Quest Software Inc.
* ALL RIGHTS RESERVED.
* Protected by U.S. Patents: 7,461,103 and 7,065,538
*******************************************************

sp_ctrl (nlam01-vtsplx01:2100)> qstatus

Queues Statistics for nlam01-vtsplx01
  Name:  Q_TO_AWS (o.SPLEX_TESTING_ONPREM-o.SPLEX_TESTING_AWS) (Post queue)
    Number of messages:          1 (Age         0 min; Size          4 mb)
    Backlog (messages):          2 (Age         0 min)

  Name:  o.SPLEX_TESTING_ONPREM (Capture queue)
    Number of messages:          3 (Age         0 min; Size          2 mb)
    Backlog (messages):          4 (Age         0 min)

  Name:  o.SPLEX_TESTING_AWS (Capture queue)
    Number of messages:          5 (Age         0 min; Size          4 mb)     0 min)
    Backlog (messages):          6 (Age         0 min)

  Name:  Q_TO_ONPREM (o.SPLEX_TESTING_AWS-o.SPLEX_TESTING_ONPREM) (Post queue)
    Number of messages:          7 (Age         0 min; Size          1 mb)
    Backlog (messages):          8 (Age         0 min)

"""
