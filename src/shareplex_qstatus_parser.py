import re


class QueueInformation:
    name = str
    source = str
    type = str
    number_of_messages = int
    backlog_messages = int


def _is_name_line(line):
    return line.find("Name:") > 0


def _get_there_is_name_in_line(line):
    return line.count('(') >= 2


def _get_queue_name_from_line(line):
    if not _get_there_is_name_in_line(line):
        return None
    return line.strip("Name:  ").split('(')[0].strip()


def _get_queue_source_from_line(line):
    if _get_there_is_name_in_line(line):
        return line.split('(')[1].replace(')', '').split('-')[0].strip()
    else:
        return line.split('(')[0].replace('Name:', '').strip()


def _get_queue_type_from_line(line):
    return line.split('(')[-1].split(' ')[0].strip()


def _is_internal_value_queue_line(line):
    return line.find("messages") > 0


def _get_queue_information_value_from_line(line):
    result = re.findall(r':\s*(\d+)', line)
    return int(result[0])


def _information_name_is_number_of_messages(line):
    return line.find("Number") > 0


def _information_name_is_backlog_of_messages(line):
    return line.find("Backlog") > 0


def parse_shareplex_output(shareplex_output):
    lines = shareplex_output.splitlines(0)
    all_queues_information = []
    queue_information = None

    for line in lines:
        if _is_name_line(line):
            if queue_information:
                all_queues_information.append(queue_information)

            queue_information = QueueInformation()
            queue_information.name = _get_queue_name_from_line(line)
            queue_information.source = _get_queue_source_from_line(line)
            if not queue_information.name:
                queue_information.name = queue_information.source

            queue_information.type = _get_queue_type_from_line(line)

        elif _is_internal_value_queue_line(line):
            information_value = _get_queue_information_value_from_line(line)

            if _information_name_is_number_of_messages(line):
                queue_information.number_of_messages = information_value
            elif _information_name_is_backlog_of_messages(line):
                queue_information.backlog_messages = information_value

    all_queues_information.append(queue_information)

    return all_queues_information
