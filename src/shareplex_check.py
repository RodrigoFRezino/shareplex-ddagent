from datadog_checks.base import AgentCheck
from datadog_checks.base.utils.subprocess_output import get_subprocess_output

from shareplex_qstatus_parser import parse_shareplex_output

__version__ = "1.0.0"


class ShareplexCheck(AgentCheck):

    def _get_qstatus_output(self):
        stdout, err, retcode = get_subprocess_output(
            ["sudo", "-s", "-u", "splex", "/opt/shareplex/splex_prod/bin/sp_ctrl", "qstatus"], self.log,
            raise_on_empty_output=True)

        return stdout

    def _send_metrics(self, qstatus_parsed):
        for queue in qstatus_parsed:
            metric_id_number_of_messages = 'shareplex.qstatus.%s.number_of_messages' % queue.type.lower()
            self.gauge(metric_id_number_of_messages, queue.number_of_messages, tags=['QUEUE_IDENTIFIER:' + queue.name])

            metric_id_backlog_messages = 'shareplex.qstatus.%s.backlog_messages' % queue.type.lower()
            self.gauge(metric_id_backlog_messages, queue.backlog_messages, tags=['QUEUE_IDENTIFIER:' + queue.name])

    def check(self, instance):
        shareplex_qstatus_result = self._get_qstatus_output()
        qstatus_parsed = parse_shareplex_output(shareplex_qstatus_result)
        self._send_metrics(qstatus_parsed)
